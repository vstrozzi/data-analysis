import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns
from pathlib import Path

# Sizes to send
SIZES = [1024, 512, 64] 
FILE_SIZE = 1048576 # 1MiB
chunks_nr = 100
# Get parent path
path = Path.cwd().resolve()
path_par = Path.cwd().parent.resolve()


# Get name of benchmark files
fname = [d for d in path.iterdir() if
       d.is_dir() and
       str(d.name).startswith("dpdk")][0]



fname = [f for f in fname.iterdir() if
        str(f.name).endswith(".txt")]

fname.sort()
# Set label
labels = [l.name.replace("_ben_proc.txt", "") for l in fname]
print(labels)
labels.sort()
data = []
# Get raw_data from loc files
for lab in fname:
    if not lab.name.startswith("host_reduce_ben_rtt.txt"):
        continue
    file = open(str(lab), "r")
    # Read benchmark for given 
    for size in SIZES:
        data_loc = []
        # Compute Throughput
        for i in range(0, chunks_nr):
            time = int(file.readline())/1000000
            # print(time)
            #if time > 100000:
             #   print(time)
             #   continue
            # Compute throughput in Gbit/s (substract overhead call time)
            data_loc.append(time)

        data.append(data_loc)
    file.close()
    break

# Create data array
data_real = data
#print(data_real)
#print(len(data_real))
# Set up the plot
ax = plt.subplot(2, 3, 1)
# Draw the plot
data = np.asarray(data_real[0])
data = np.asarray([d for d in data])
sns.boxplot(data=data, hist=True, kde=True, 
            bins=30 , color = 'darkblue', 
            hist_kws={'edgecolor':'black'},
            kde_kws={'linewidth': 1})

# Title and labels
ax.set_title("1 MiB in 1024B packets", size = 11)
ax.set_xlabel('Delay (ms)', size = 11)
ax.set_ylabel('Probability Density Function', size= 11)
ax.set_xlim([0, 2*data.mean()])
ax.axvline(data.mean(), color='k', linestyle='dashed', linewidth=1, label="Mean {:.2f}".format(data.mean()))
ax.axvline(np.median(data), color='r', linestyle='dashed', linewidth=1, label="Median {:.2f}".format(np.median(data)))
ax.plot([], color="w", label="Std {:.2f}".format(data.std()))
ax.grid(axis='y')
ax.legend(loc=0)

ax = plt.subplot(2, 3, 4)
sns.distplot(data, hist=True, kde=True,
            bins=30, 
            hist_kws={'cumulative': True},
            kde_kws= {'cumulative': True}
            )

plt.tight_layout()
plt.show()

plt.savefig('filename.png', dpi=300)
